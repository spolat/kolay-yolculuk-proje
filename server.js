const express = require('express'),
      dev = require('./config/development'),
      prod = require('./config/production'),
      bodyParser = require('body-parser'),
      request = require('request'),
      asyncLoop = require('node-async-loop'),
      logger = require('morgan'),
      PORT = dev.port || prod.port,
      devURL = "http://candidate.kolayyolculuk.com/dev",
      prodURL = "http://candidate.kolayyolculuk.com/prod",
      app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : true}));
app.use(logger('dev'));

app.post('/searchProd' , (req,res) => {

  //Gidiş-Dönüş birlikte aratılıyor.
  if(req.body.departureDate && req.body.returnDate)
  {
    const ucuslar = [];
    const from = req.body.from;
    const to = req.body.to;
    const departureDate = req.body.departureDate;
    const returnDate = req.body.returnDate;

    const myData = {
      from,
      to,
      departureDate,
      returnDate
    };

    request.post({url : prodURL , formData : myData} , function optionalCallback(err, httpResponse, body){
      if(err) return res.status(500).json(err);
      const connectionsObject = JSON.parse(body);
      const connections = connectionsObject.connections;
      const flights = connectionsObject.flights;
      const departureFlights = connectionsObject.flights[0];
      const returnFlights = connectionsObject.flights[1];
      const responseArray = [];

      asyncLoop(connections , (item, next) => {
        const departureOrderNo = item.orders[0];
        const returnOrderNo = item.orders[1];

        const departureFlight = flights[0][departureOrderNo-1];
        const returnFlight = flights[1][returnOrderNo-1];
        const departureFlightNo = departureFlight.airline + departureFlight.number;
        const returnFlightNo = returnFlight.airline + returnFlight.number;
        const fullPrice = departureFlight.price + returnFlight.price;
        const price = (item.price).toFixed(2);
        const savings = Math.round(fullPrice - price) + 1;
        const flight = {
          from,
          to,
          price,
          savings,
          departureFlightNo,
          returnFlightNo
        };
        responseArray.push(flight);
        next();
      } , () => {
        //loop finished
        return res.status(200).send(responseArray);
      });
    });
  }
  //Geçersiz arama
  else if(!req.body.departureDate && !req.body.returnDate){
    return res.status(500).send('Geçersiz arama');
  }
  //Tek yon arama yapılıyor
  else if(!req.body.returnDate){
    const ucuslar = [];
    const responseArray = [];
    const from = req.body.from;
    const to = req.body.to;
    const departureDate = req.body.departureDate;

    const myData = {
      from,
      to,
      departureDate
    };
    request.post({url : prodURL , formData : myData} , function optionalCallback(err, httpResponse, body) {
      const flights = JSON.parse(body).flights[0];
      asyncLoop(flights , (item,next) => {
        const departure = item.airline + item.number;
        const price = item.price;
        const flight = {
          from,
          to,
          departureDate,
          price
        };
        responseArray.push(flight);
        next();
      }, () => {
        return res.status(200).send(responseArray);
      });
    });
  }
});

app.post('/searchDev' , (req,res) => {

  //Gidiş-Dönüş birlikte aratılıyor.
  if(req.body.departureDate && req.body.returnDate)
  {
    const ucuslar = [];
    const from = req.body.from;
    const to = req.body.to;
    const departureDate = req.body.departureDate;
    const returnDate = req.body.returnDate;

    const myData = {
      from,
      to,
      departureDate,
      returnDate
    };

    request.post({url : devURL , formData : myData} , function optionalCallback(err, httpResponse, body){
      if(err) return res.status(500).json(err);
      if(JSON.parse(body).connections)
      {
        const connectionsObject = JSON.parse(body);
        const connections = connectionsObject.connections;
        const flights = connectionsObject.flights;
        const departureFlights = connectionsObject.flights[0];
        const returnFlights = connectionsObject.flights[1];
        const responseArray = [];

        asyncLoop(connections , (item, next) => {
          const departureOrderNo = item.orders[0];
          const returnOrderNo = item.orders[1];

          const departureFlight = flights[0][departureOrderNo-1];
          const returnFlight = flights[1][returnOrderNo-1];
          const departureFlightNo = departureFlight.airline + departureFlight.number;
          const returnFlightNo = returnFlight.airline + returnFlight.number;
          const fullPrice = departureFlight.price + returnFlight.price;
          const price = (item.price).toFixed(2);
          const savings = Math.round(fullPrice - price) + 1;
          const flight = {
            from,
            to,
            price,
            savings,
            departureFlightNo,
            returnFlightNo
          };
          responseArray.push(flight);
          next();
        } , () => {
          //loop finished
          return res.status(200).send(responseArray);
        });
      }
      else{
        return res.status(500).send('An error occured please try again ' + body);
      }
    });
  }
  //Geçersiz arama
  else if(!req.body.departureDate && !req.body.returnDate){
    return res.status(500).send('Geçersiz arama');
  }
  //Tek yon arama yapılıyor
  else if(!req.body.returnDate){
    const ucuslar = [];
    const responseArray = [];
    const from = req.body.from;
    const to = req.body.to;
    const departureDate = req.body.departureDate;

    const myData = {
      from,
      to,
      departureDate
    };
    request.post({url : devURL , formData : myData} , function optionalCallback(err, httpResponse, body) {
      if(err) return res.status(500).send(err);
      if(JSON.parse(body).connections){
        const flights = JSON.parse(body).flights[0];
        asyncLoop(flights , (item,next) => {
          const departure = item.airline + item.number;
          const price = item.price;
          const flight = {
            from,
            to,
            departureDate,
            price
          };
          responseArray.push(flight);
          next();
        }, () => {
          return res.status(200).send(responseArray);
        });
      }
      else{
        return res.status(500).send('An error occured please try again ' + body);
      }
    });
  }
});


app.listen(PORT , () => {
  console.log('App listening on port: ' + PORT);
});
